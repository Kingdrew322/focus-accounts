Intro letter to Investors

Dear {{investor_fname}},

I happened to notice on your LinkedIn profile that you’re connected to Mike Amend who just joined Lowe’s as their President of Online. They are currently modernizing their architecture, process maturity, and capabilities - and are exactly in the right position to benefit from GitLab’s application. 

Would you be willing to introduce us? If so I have attached a template for your review. Please feel free to edit and change it in any way you want.

Warm Regards,
Kevin McKinley

****

Mike, this is Kevin. I wanted to take the opportunity to introduce the two of you. Mike is a good friend of mine, and Kevin works for one of our investment companies (GitLab) He’s written a letter explaining how GitLab can help with the digital transformation you’re looking to achieve at Lowe’s, which I’ve attached below. Kevin, I would ask you reach out to Mike and set up a time to speak. If either of you want me to be part of that conversation, or have any questions, please reach out.

*****

To: Mike Amend, President - Online, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2018

Subject: Why companies like Macy’s and Dell use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Amend,

I hope this missive finds you well. It is with tremendous respect and admiration that I reach out to you today as I have been following your work for quite some time. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The responsibilities you have to transform the online business and strategy at Lowe’s (doubtless because of your previous successes in transforming the Omnichannel organization and innovating at JCP) are precisely what GitLab enables. GitLab is touted as being the fastest and most reliable way for Presidents to enable digital transformation throughout their respective companies. Here’s why…

The GitLab application is used by over 100,000 enterprises worldwide to modernize their architecture, embrace cloud-native technologies, and increase velocity of innovation.

Mr. Amend, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your IT deliverables with business value through "Value Stream Mapping" thus creating visibility into your pipeline
2. We can increase your efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, let’s arrange a brief phone call to see if our companies would make for a great partnership. I will call your office on Friday, Feb 1 at 11:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
