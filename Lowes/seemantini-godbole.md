To: Seemantini Godbole, Chief Information Officer, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2019

Subject: Why the CIOs of companies like Macy’s and Expedia use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Ms. Godbole,

I hope this missive finds you well. It is with tremendous respect and admiration that I reach out to you today as I have been following your work for quite some time. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you have proposed during Lowe's 2018 Analyst & Investor Conference to lead Lowe’s large scale transformational efforts (especially in architecture, process maturity, and capabilities) are precisely what GitLab enables. GitLab is touted as being the fastest and most reliable way for CIOs to enable digital transformation throughout their respective companies. Here’s why…

The GitLab application is used by over 100,000 enterprises worldwide to modernize their architecture, embrace cloud-native technologies, and increase velocity of innovation.

Ms. Godbole, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can align your IT deliverables with business value and increase customer engagement through "Value Stream Mapping" and creating visibility into your pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, let’s arrange a brief phone call to see if our companies would make for a great partnership. I will call your office on Friday, Feb 1 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

****

1. We can align your IT deliverables with business value through "Value Stream Mapping" thus creating visibility into your pipeline
2. We can increase your efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can reduce IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

