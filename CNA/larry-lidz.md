To: Larry Lidz, SVP & CISO, CNA Insurance
CNA Financial
333 S Wabash
Chicago, IL 60604 US


24 January 2019

Subject: Why companies like Goldman Sachs use Gitlab to defend and secure all aspects of their Information Security

Warm greetings and happy New Year, Mr. Lidz,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During the last several months, I've learned by speaking with three of your IT Directors that there is broad usage of GitLab's application at CNA. Surprisingly, however, they informed me that you are not yet leveraging it's security capabilities.

I bring this to your attention because I beleive that utilization of GitLab's security features can dramatically assist your organization in securing CNA's technology infrastructure and protecting critical customer information. GitLab is touted as being the fastest and most reliable way for CISOs to shift security left throughout their respective companies, and is precisely why Goldman Sachs has adopted our solution.

Mr. Lidz, I believe there are at least four primary ways GitLab can help you defend and secure Lowe's information technology while continuing to drive value for your stakeholders.

1. We can create visibility into your entire pipeline by having a Single Application for the entire Software Development Lifecycle
2. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less 
3. We can address vulnerabilities and enhance information security with automated testing and code scanning
4. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
