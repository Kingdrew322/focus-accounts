•	Highly effective hands-on IT leader with 20+ years of experience in the P&C insurance industry and an exceptional mix of client service, operations and technology responsibilities. 10+ years of experience in implementing Guidewire Insurance products. 3+ years of experience in implementing Guidewire Claim Center projects, 4+ years of experience in implementing Claims severity, and Policy pricing models and 8+ years of experience in implementing Guidewire Policy Center projects
•	UIC MBA with a passion for entrepreneurial endeavors and identifying innovative ways to drive scalability, optimize operational costs, and drive top line performance. 
•	High-profile roles include presenting to 60-200+ people and facilitating workshops and focus groups of 8-30 people and managed 100 – 300 resources
•	Client focused professional with a strong understanding of business needs assessment, IT delivery methodologies and technologies
•	Proven history of fostering strong relationships between Business and IT executives

Accomplishments: Evaluated and prioritized Underwriting transformation project, and sequenced onto a 4-year, $330M+ roadmap. Completed 1.5 years of the roadmap which included Policy Center 8.0 upgrade, migration of Workers Comp including Retros, Large deductibles, Middle and Small business, Supported Umbrella, Manoline Umbrella and Life Sciences products. Total of $1.1 B business migrated to Policy Center 8.0 See less


****


To: Radha Kanakamedala, Vice President Of Application Development, CNA Insurance
CNA Financial
333 S Wabash
Chicago, IL 60604 US


24 January 2019

Subject: Why companies Goldman Sachs use Gitlab to to manage their IT Infrastructure
 
Warm greetings and happy New Year, Ms. Kanakamedala,

I hope this missive finds you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During the last several months, I've learned by speaking with three of your IT Directors that you are moving CNA to cuttingcloud native technologies, modernizing your applications architecture, and adopting DevOps throughout the enterprise. As these initiatives are precisely what our application enables there is (unsurpisingly) broad usage of the GitLab application at CNA. However, I was surprised to learn that this usage is limited to one aspect of the application, and that CNA is not yet leveraging the full breadth of the product's capabilities.

I bring this to your attention because I beleive that further utilization of GitLab's other features can dramatically accelerate your digital transformation and the delivery of your proposed projects. Those key areas of large-scale IT implementations and transformation is why GitLab is used by clients like ING Bank, BNY Mellon, and Freddie Mac.

As a concrete example, Goldman Sachs was in a similar situation and looking to modernize their applications and infrastructure -- which meant they were slow to innovate and sought to speed up their delivery. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*


Ms. Kanakamedala, I believe there are at least five primary ways GitLab can help you drive top-line performance and scalability as well as optimize operational costs for your customers and stakeholders:

1. We can align your IT deliverables with business value and increase customer engagement through "Value Stream Mapping" and creating visibility into your pipeline
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can minimize IT costs and overhead which also means less time spent integrating and maintaining your tools 
4. We can address vulnerabilities and enhance information security with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
