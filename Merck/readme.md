# Focus Account

## Company Background:

Merck & Co.
351 N Sumneytown Pike
North Wales, PA 19454 US

Merck & Co.
1 Merck Drive
Whitehouse Station, NJ 08889 US


## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?
1. Why Gitlab
2. Why Now
3. Why Do Anything At All?


## Identify C-Suite:

1. Jim Scholefield - Executive VP, Chief Information & Digital Officer 
2. Marc Raman - CIO Global Human Health (EUCAN & Emerging Markets)
3. Michele D'Alessandro - Vice President & Chief Information Officer, Manufacturing IT
4. Phyllis Post - Chief Information Officer, Global Human Health IT
5. Terry Rice - Assistant VP, IT Risk Management & CISO
6. Sandy Tremps - Vice President, R&D IT
7. James Ciriello - VP, Digital Strategy and CIO of Emerging Businesses 


Hal Stern
Peter Lega - Director, Applied Architecture & Emerging Technology
Ken Griggs - Global DevOps Lead, https://www.linkedin.com/in/ken-griggs-6a99941/
Rich Murray - Director, IT Strategy & Architecture
Nitin Kaul
Richard Murray
Drew Kimberlin
Dave Kniaz
Andy Porter
Keith Heilner


## Write C-Suite Letters

## Create Outreach Campaign for Directors


## Call Log:

* What do you know about Gitlab?
* Role
* Size 
* Situation
* Problem 
* Goals
* Budget
* Authority
* Timeline	
* Questions
* Next Steps

