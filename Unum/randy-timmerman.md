
To: Randy Timmerman, VP of Application Development
Unum
1 Fountain Square
Chattanooga, TN 37402


February 1, 2019

Subject: Why do companies like Goldman Sachs use Gitlab? Because it's the fastest way for CIOs to deliver a digital transformation

Warm greetings Mr. Timmerman,

I hope the new year has treated you well. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to guide Unum through a Digital Transformation journey, as well as architect DevOps and cloud capabilities are precisely what the GitLab application enables.

I bring this to your attention because I believe that utilization of GitLab within Unum can dramatically accelerate the delivery of your digital transformation and proposed objectives. Those key areas of continuous integration, continuous delivery, and continuous testing, is why GitLab is used by clients like ING Bank, BNY Mellon, and Freddie Mac.

As a concrete example, Goldman Sachs was in a similar scenario within their digital transformation. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to be more Agile and produce faster than they ever thought possible.

*“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs*

Mr. Timmerman, I believe there are at least five primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can enable better collaboration between IT and the business, resulting in alignment of your IT deliverables with business value and doing the right thing for the end customer  
2. We can improve your productivity and operational efficiency by having a Single Application for the entire Software Development Lifecycle, which means being able to produce new products significantly faster
3. We can help you be fiscally responsible by reducing IT costs and overhead which also means less time spent integrating and maintaining your tools
4. We can enhance information security and remediate vulnerabilities with automated testing and code scanning
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your objectives and our solution would make for an ideal partnership. I will call your office on Friday, Feb 8 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

*****

Hired to establish and accelerate growth for Unum’s ability to deliver enterprise (horizontal based) middleware technical capabilities related to Data (data warehouse, data integration, MDM and business intelligence), Service Oriented Architecture (integration, web services, enterprise service bus), Business Process Management (human and automated workflow, business activity monitoring) and Enterprise Content Management. 

Highlights:
Developed and deployed Unum’s first Architecture Operating Model – accounting for strategic, domain (data, integration, security, network, infrastructure) and solution architecture disciplines, governance and software delivery architecture disciplines.
Visionary leader guiding Unum through a Digital Transformation journey, focused on greenfield and brownfield architectural capabilities, delivering on an Unum US Business Operating Model that addresses a multi-channel approach to delivering outstanding customer experience with Unum’s products and service delivery. 
Lead architect on Unum’s Majesco Policy Admin Architecture runway team – evaluated, deployed and support the full architectural deployment of the platform. Architected the platform that allowed for weekly releases focused on DevOp capabilities including Azure cloud-based capabilities, Software-as-Code and automated continuous integration (CI), continuous deployment (CD) and continuous test.
Established and implemented Unum’s Enterprise Service Bus utilizing IBM’s IIB v9.0 and MQ product suite of tools.
Deliver on Unum’s Enterprise Data Architecture Strategy, establishing 4 layered approach to data including source systems, trusted information (based on IBM DB2 and IBM MDM), analytical information (based on Teradata Data Warehouse platform) and business intelligence (based on QlikView and Cognos platforms).
Key leader in Unum’s Application Portfolio Simplification effort to reduce number of systems from 800 to 400 and reduce run rate expenditure by $60 million.

Multi-faceted, innovative, and customer-driven executive, offering commendable data/technology experience within a variety of diverse industries with a focus on large program/project management, applications and infrastructure strategy/implementation, data architecture, core business processes, software quality assurance, data & regulatory compliance and software development methodologies. Astute ability to integrate IT solutions with business strategies and a strong drive to achieve and excel at all levels. Accomplished strategic leader, with consistent business objective delivery success and process improvement achievement; highly recognized technology executive for integrating business capabilities and application/infrastructure development background into the corporate world.

Thrive in environments that present technology challenges.
Demonstrate ability to lead large-scale technical teams, with powerful problem solving and troubleshooting, time management, and exceptional communication skills.
Highly discrete and dependable technology leader, with keen attention to detail and unswerving commitment to the highest standards of professional and personal service.

