# Focus Account

## Company Background:



## Account Research:

Why's building & execution: How effectively do you determine the 3 Why's of doing a deal?
1. Why Gitlab
2. Why Now
3. Why Do Anything At All?

Automation in two buckets. Automation is fundamental to improve productivity and efficiency within the company. I fundamentally believe that if our operations are efficient, it results in great customer service so productivity and customer service go hand in hand.

The other bucket are applications that we are developing from scratch. Those we are fundamentally designing all of them to be straight through processing, closed loop communications with our customers, exactly how one would expect new applications to be built.

The role has changed from pure technologist to be a business technologist. As the consumerization of technology continues to increase, the distinction between a business individual and a technology individual is going to further shrink. The traditional role of the CIO will fundamentally disappear. 

When you look at digital transformation, we are fundamentally focused on four different things. One is customer experience. And customer experience is a whole lot more than just a great website or a great mobile application. Our second strategy is around productivity and efficieny and improvements. The third thing is expanding our ecosystem. We think of ourselves as a player that is providing a service - what we want to do is be at the center of the customer experience. It does not matter who provides the service to the customer, as long as we manage and are central to how that service is provided. The last thing in terms of our strategy is building out a growth platform. When you look at insurance companies in general, we all are burdened with legacy technology. We have this approach where while we are still trying to address working around the legacy environment - we call it the "digital surroung strategy" - in parallel with that we are investing in much more nimble platforms that allow us to price new products significantly faster than we have done in the past.

I think of technology as a value enable. Always think about the end customer first. If you do the right thing for the end customer, other things will naturally fall into place. What drives me and what I believe should drive individuals in my department, is doing the right thing for the end customer. The second is around teamwork and collaboration. As a team, we can achieve much greater things. Collaboration both within IT, and between IT and the business becomes really important. Fiduciary responsibility is really important. Make good decisions, take risks, if you're going to fail, fail quickly and move on.


## Identify C-Suite:

1. Puneet Bhasin - CIO and CDO
2. Mike Simonds President and Chief Executive Officer, Unum US
3. Randy Timmerman - VP of Application Development
4. Lynda Fleury -Chief Information Security Officer (EA is Deanna Chastain)
5. Preetha Sekharan, Vice President, Digital Strategy & Transformations
6. Sydney Crisp - VP of  (EA is Deanna Chastain)

* Stan Dowd, Vice President, Information Technology (Colonial Life)
* Elizabeth Jessen Assistant VP, Information Security, Risk Management & Cost Management
* David Rector, Director of App Dev
* Marci Cyr, Assistant VP, Project Management Office
* Blake Pease, Assistant VP, Infrastructure Architecture & Engineering
* Cliff Johnson, Assistant VP, Enterprise Architecture
* David McMahon, Vice President, Operation Services
* Kathi O'Grady, Vice President, Application Management
    

## Write C-Suite Letters

## Create Outreach Campaign for Directors


## Call Log:

* What do you know about Gitlab?
* Role
* Size 
* Situation
* Problem 
* Goals
* Budget
* Authority
* Timeline	
* Questions
* Next Steps

