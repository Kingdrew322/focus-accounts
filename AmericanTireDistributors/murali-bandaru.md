Murali Bandaru
Senior Vice President, Applications & Technology
American Tire Distributors
12200 Herbert Wayne Court
Huntersville, NC 28078

7 February 2019


Warm greetings Mr. Bandaru,

I hope this missive finds you well and that the new year has been treating you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with TireBusiness, Stuart Schuette's mentioned that ATD's goal is to create an "efficient digital ecosystem" and using digital technology to drive "game-changing innovation." I bring this to your attention because innovation and an efficient ecosystem is exactly what GitLab enables. Furthermore, the initatives you are engaged in to guide ATD through a Digital Business transformation and implement DevOps, Microservices Architecture, and Cloud Native Development, and GCP are why clients like Michelin use GitLab. I believe our application can accelerate your delivery of these improvements.

As a concrete example, Goldman Sachs was in a similar situation with an outdated techonology toolset -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible. (Teams that were releasing software every 2 weeks are now releasing 6x a day.)

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs

Mr. Bandaru, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can increase your operational efficiency and speed to market by using a Single Application for the entire Software Development Lifecycle
2. We can allow faster innovation velocity by aligning your IT deliverables with customer feedback through visibility into your entire pipeline
3. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so your teams can focus on delivering software
4. We can address vulnerabilities and enhance information security with automated testing and code scanning 
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,



Kevin McKinley
