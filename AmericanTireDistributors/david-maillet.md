David Maillet
Director of Devops, QA, and Performance Engineering
American Tire Distributors
12200 Herbert Wayne Court
Huntersville, NC 28078

7 February 2019


Warm greetings Mr. Maillet,

I hope this missive finds you well and that the new year has been treating you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with TireBusiness, Stuart Schuette's mentioned that ATD's goal is to create an "efficient digital ecosystem" and using digital technology to drive "game-changing innovation." I bring this to your attention because innovation and an efficient ecosystem is exactly what GitLab enables. Furthermore, the initatives you are engaged in to guide ATD's IT Operations and Infrastructure are why clients like Michelin use GitLab. I believe our application can accelerate your delivery of these improvements.

You mention on LinkedIn that you came to ATD to "restart and refocus DevOps, Release Management, Quality Engineering (QE), and Performance Engineering (PE) teams". These initiatives are precisely why clients like Michelin and Dillard's use GitLab, and as such I believe our application can accelerate your delivery of these improvements.

As a concrete example, Goldman Sachs was in a similar situation with an outdated techonology toolset -- which meant they were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible. But don't take it from me. Here's what their Technology Fellow had to say:

“Our teams went from two weeks releases to six times per day using GitLab because they do not need to wait for infrastructure." Andrew Knight, Managing Director, Goldman Sachs

Mr. Maillet, I believe there are at least five primary ways GitLab can help you drive value for your customers and stakeholders:

1. DevOps - GitLab is the first single application for the entire DevOps lifecycle, which means you only need one tool instead of the typical stack. Why integrate and maintain JIRA + SCM + CI + etc. if one application does it better?
2. Release Management - As stated above, by using GitLab, clients have been able to go from two week releases to releasing 6x a day, because this allowed self-service and automation.
3. Quality Assurance - GitLab includes automated testing and code scanning, so that every branch and merge request gets tested. This allowed Paessler AG to reduce each QA engineer's tasks from 1 hour per day to only 30 seconds per day.
4. Performance Engineering - You mentioned Site Reliability, and if you have intentions of utilizing GCP or Kubernetes, GitLab can integrate with and manage those aspects including canary and blue-green deployments.
5. Minimize IT Costs - Most of our clients find GitLab to be less expensive than the typical chain of tools, not to mention the reduction in the overhead costs of having to maintain, integrate, and manage licenses for mutliple tools

If these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,



Kevin McKinley


****

I came to American Tire Distributors to restart and refocus DevOps, Release Management, Quality Engineering (QE), and Performance Engineering (PE) teams. We are implementing software engineering practices that improve how we work together and accelerate solution delivery. The role is also interesting because the type of business, tires and tire accessories, differs quite a bit from the department store business from my prior role of 8+ years.

Volunteered to lead three different teams that worked closely together to help other teams deliver application features more successfully: DevOps, Performance Engineering, and QA Automation. For DevOps, leveraging Continuous Integration/Delivery tools to improve software development quality while decreasing application delivery time. Led a team that focused on performance engineering on several core enterprise applications. Facilitated automated testing in order management systems.

Leading a team with three distinct focuses: performance engineering, QA automation, and DevOps practices. Contributing to site reliability and backend application quality. Providing self-service and automation of repetitive tasks with various systems, freeing humans to focus on the really interesting and creative stuff.

Led a team that delivered DevOps processes and Continuous Integration/Delivery tools aimed to improve software development quality while simultaneously decreasing the time it takes for application deployments. Installed, configured, and deployed new Continuous Integration tools and accompanying processes. Provided Release Management, including release planning, release acceptance, and deployment planning. Planned and facilitated with application architecture and infrastructure engineering to meet the needs for Development, QA, and Production environments. See less



