Ryan Marsh 
Growth Chief & Innovation Officer
American Tire Distributors
12200 Herbert Wayne Court
Huntersville, NC 28078

February 12, 2019


Warm greetings Mr. Marsh,

I hope this missive finds you well and that the new year has been treating you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

During a recent interview with TireBusiness, Stuart Schuette's mentioned that ATD's goal is to create an "efficient digital ecosystem" and using digital technology to drive "game-changing innovation." I bring this to your attention because innovation and an efficient ecosystem is exactly what GitLab enables. I believe our application can accelerate your delivery of these improvements, just as we've done for clients like Michelin.

Mr. Marsh, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can increase your operational efficiency and speed to market by using a Single Application for the entire Software Development Lifecycle
2. We can allow faster innovation velocity by aligning your IT deliverables with customer feedback through visibility into your entire pipeline
3. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so your teams can focus on delivering software
4. We can address vulnerabilities and enhance information security with automated testing and code scanning 
5. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether Lowe's and our solution would make for a great partnership. 

If these outcomes sound like they might be relevant to your world, may I suggest we arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 19 at 9:30 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley

*****


Mr. Schuette said ATD's goal for the next six months is to become an intelligent digital distributor for retailers and manufacturers, connecting consumers to dealers, creating an "efficient digital ecosystem."
The firm plans to accomplish that by:

Investing in knowledge;
Monitoring the market;
Leveraging exceptional commercial capabilities;
Boosting manufacturing technology; and
Embedding technology to create competitive advantages in the long term.
"We're excited about the future, particularly in the area of digital technology," Mr. Schuette said. "As we look ahead, we'll be driving game-changing innovation. ATD is — and will continue to be — the leader in the replacement tire business because of our commitment to focusing on innovation to deliver the very best experience for our customers.

"We know we can achieve that through the continued dedication of our associates. We can't wait for tomorrow.




"We're excited about the future, particularly in the area of digital technology," Mr. Schuette said. "As we look ahead, we'll be driving game-changing innovation. ATD is — and will continue to be — the leader in the replacement tire business because of our commitment to focusing on innovation to deliver the very best experience for our customers.



“The world is changing,” said Stuart Schuette, president and CEO of ATD, in his presentation at the Tire Pros event. “We need to stay ahead of that growth in innovation. We actually have now created a company within a company; we have a growth and innovation organization put in place at ATD. Their focus is on how to help you as an independent dealer.” "separate company thinking about what we need to do to build things out for our dealer partners"


During Lowe's 2018 Analyst & Investor Conference, you mentioned six key areas of focus which require IT system improvements. Those key areas of omni-channel, operational efficiency, and customer engagement are precisely what GitLab enables. I believe our application can accelerate your delivery of these improvements, just as we've done for clients like Macy's.

GitLab is touted as being the fastest and most reliable way for CEOs to enable digital transformation throughout their respective companies. As a concrete example, Goldman Sachs was in a similar situation with an outdated techonology toolset -- which meant they couldn't attract the best talent and were slow to innovate. Over the last year, they've migrated 5,000 developers to GitLab, which has resulted in an 84x increase in build productivity and allowed them to deploy faster than they ever thought possible. (Teams that were releasing software every 2 weeks are now releasing 6x a day.)

Mr. Ellison, I believe there are at least four primary ways GitLab can help you drive value for your customers more quickly than your competitors.

1. We can improve your customer engagement (especially with the Pros) by aligning your IT deliverables with what the customers want
2. We can increase your operational efficiency, innovation velocity, and speed of delivery by having a Single Application for the entire Software Development Lifecycle
3. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less
4. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether Lowe's and our solution would make for a great partnership. I will call your office on Friday, Feb 1 at 9:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
