
To: Warren Steytler, CISO, Lowe’s
1000 Lowe’s Boulevard
Mooresville, NC 28117
24 January 2019

Subject: Why the CISOs of companies like Macy’s and Expedia use Gitlab to adopt enterprise-wide digital transformation
 
Warm greetings and happy New Year, Mr. Steytler,

I hope this missive finds you well, and congratulations in acquiring your US Citizenship. 

I am reaching out to you today because in my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

The initiatives you are engaged in to secure Lowe’s technology infrastructure and protect critical customer information are precisely what the GitLab application enables. GitLab is touted as being the fastest and most reliable way for CISOs to shift security left throughout their respective companies. 

The GitLab application is used by over 100,000 enterprises worldwide to remove complexity that increases risk, and is the tool to make processes happen faster and more easily.

Mr. Steytler, I believe there are at least four primary ways GitLab can help you defend and secure Lowe's information technology while still driving value for your customers.

1. We can create visibility into your entire pipeline by having a Single Application for the entire Software Development Lifecycle
2. We can minimize IT costs and overhead which means less time spent integrating and maintaining your tools so you can Do It Right For Less 
3. We can address vulnerabilities and enhance information security with automated testing and code scanning
4. We can make Regulatory Compliance easy with an IT Audit Trail created by having a "Single Source of Truth"

If these outcomes sound like they might be relevant to your world, then a brief phone call may be in order. Together, we can quickly assess whether your initiatives and our solution would make for an ideal partnership. I will call your office on Friday Feb 1 at 10:00 AM EST. If this is an inconvenient time, kindly inform me as to your preferred availability. 

To your continued success,

Kevin McKinley
