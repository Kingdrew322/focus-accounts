John Rzeszotarski
SVP & Director of Continuous Delivery and Feedback
KeyBank
127 Public Square
Cleveland, OH 44114

Warm greetings Mr. Rzeszotarski,

I hope this missive finds you well, and that the new year has treated you kindly. In my role as an SDR for GitLab, it is my responsibility to identify and research suitable partners who may benefit from our organization. 

In your article, "I Find Your Lack of Faith Disturbing" you write that "At the heart of it all, the message is that we care about doing the right thing for people. [....] The biggest benefit of DevOps is not talked about enough, which is removing bias and making ethical decisions not just for the business, but for the people as well.

I bring this to your attention because this mission of doing the right thing is what our appliction seeks to enable for the enterprise. By aligning deliverables with what the customers really want, and by trusting the developers and putting power back into their hands with tools that allow autonomy. 

Don't take it from me, however. Here's what our partners have to say: 

“We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software.” - George Grant, VP & Technology Fellow, Goldman Sachs

"GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!” Andrew Knight, Managing Director & Technology Fellow at Goldman Sachs

"We were just spending too much time doing stuff manually, so we decided to just start fresh and write everything from scratch. We wanted easily repeatable, with zero-touch, zero-downtime deployments, automated tracking. GitLab replaced a bunch of disparate systems for us like Jira, BitBucket, and Jenkins. GitLab provided us with a one-stop solution." Mohammed Mehdi, Principal DevOps, Verizon

“GitLab takes the culture of the community and brings it to where you can actually codify how humans can interact together well. That’s difficult to capture, and I think GitLab does a really excellent job of not forcing people but really encouraging a collaborative beneficial work environment.” Chris McClimans, Cross-Cloud CI Project Co-Founder

Mr. Rzeszotarski, I believe there are at least four primary ways GitLab can help you drive value for your customers and continue to lead Key’s digital transformation journey.

I won't ruin this letter by talking about business cases like speed, cost, or reliability, but if any of what I've written above sounds like it might be relevant to your world, I would like to suggest for us to arrange a brief phone call. Together, we can quickly assess whether our companies would make for an ideal partnership. 

I will call your office on Tuesday, Feb 26 at 9:00 AM EST to ensure you've received this letter and answer any questions you may have. If this is an inconvenient time, kindly inform me as to your preferred availability.  

To always doing the right thing,




Kevin McKinley

